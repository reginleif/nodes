;;;; solid-engine-example.asd

(asdf:defsystem #:nodes
  :description "Demo-application of solid-engine (Common Lisp stack-based application controller)"
  :author "Makarov Alexey <alexeys9@yandex.ru>"
  :license "MIT"
  :version "1.0"
  :serial t
  :defsystem-depends-on (#:closure-template)
  :depends-on (#:closure-template
	       #:hunchentoot
	       #:solid-engine)
  :components ((:file "package")
	       (:closure-template "page" :type "soy")
	       (:closure-template "node" :type "soy")
	       (:closure-template "root" :type "soy")
	       (:closure-template "child" :type "soy")
	       (:closure-template "new-node" :type "soy")
	       (:file "nodes")))
