# Nodes

Demo application for solid-engine (the Common Lisp stack-based application controller)

## Installing:

```shell
cd ~/quicklisp/local-projects
git clone https://reginleif@bitbucket.org/reginleif/nodes.git
```

## Running:

```common-lisp
(ql:quickload :nodes :verbose t)
(nodes:start-nodes :port 8080)
```

Then run your browser and type address: [localhost:8080](http://localhost:8080)
