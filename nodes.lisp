;;;; nodes.lisp

(in-package #:nodes)

(setf *catch-errors-p* t)
(setf *show-lisp-errors-p* t)
(setf *show-lisp-backtraces-p* t)

;;; node.lisp

(defvar *root*)

(defclass node ()
  ((id :reader id-of)
   (children :initform (list)
	     :accessor children-of)))

(defmethod initialize-instance :after ((instance node) &key (root instance) &allow-other-keys)
  (with-slots (id)
      instance
    (setf id (sxhash instance)))
  (push instance (nodes-of root)))

(defclass child-node (node)
  ((parent :initarg :parent
	   :reader parent-of)
   (name :initarg :name
	 :reader name-of)))

(defmethod initialize-instance :after ((instance child-node) &key parent &allow-other-keys)
  (push instance (children-of parent)))

(defun add-child (parent name &optional (root *root*))
  (make-instance 'child-node :root root :parent parent :name name))

;;; application

(defclass application (acceptor node)
  ((nodes :initform (list)
	  :accessor nodes-of)))

(defun start-nodes (&rest args &key &allow-other-keys)
  (start
   (apply #'make-instance 'application :name "Tree" args)))

(defmethod acceptor-dispatch-request ((acceptor application) request)
  (let ((script-name (script-name* request)))
    (cond
      ((or
	(starts-with-subseq "/img" script-name)
	(string= "/favicon.ico" script-name))
       (call-next-method))
      (t
       (let ((commands (make-path script-name))
	     (parameters (list-parameters request)))
	 (with-solid-engine (commands parameters)
	     ((:command-is-expected (&rest args &key &allow-other-keys)
		(apply #'handle-view args))
	      (:value-is-expected (&rest args &key &allow-other-keys)
		(apply #'handle-view args))
	      (:end-of-stack (&rest args &key &allow-other-keys)
		(apply #'handle-view args)))
	   (let ((root acceptor))
	     (main root))))))))

(defun handle-view (&key view arguments commands parameters &allow-other-keys)
  (let ((script-name
	 (format nil "/~{~(~a~)~^/~}" commands)))
    (case (request-method*)
      (:get
       (render-view view arguments script-name parameters))
      (:post
       (redirect (format nil "~a~@[?~{~(~a~)=~a~^&~}~]"
			 script-name (alist-plist parameters))
		 :code +http-see-other+)))))

(defun render-view (view arguments script-name parameters)
  (let ((*injected-data*
	 (list :script-name script-name
	       :query-string (format nil "~{~(~a~)=~a~^&~}"
				     (alist-plist parameters))
	       :parameters (mapcar #'(lambda (parameter)
				       (destructuring-bind (name . value)
					   parameter
					 (list :name name
					       :value value)))
				   parameters)))
	(template
	 (symbol-function
	  (find-symbol
	   (string view)
	   (find-package "NODES.VIEW"))))) ;; view package
    (funcall template arguments)))

(defun make-path (script-name)
  (let ((package (find-package "NODES"))) ;; command package
    (mapcar #'(lambda (name)
		(let ((symbol-name (string-upcase name)))
		  (or
		   (find-symbol symbol-name package)
		   (error "Symbol path with name '~a' not found in package ~a"
			  name package))))
	    (split "/+" (subseq script-name 1)))))

(defun list-parameters (request)
  (case (request-method* request)
    (:get (get-parameters* request))
    (:post (post-parameters* request))))

;;; main

(defun find-object-by-id (param-name)
  (let ((id (parameter-value param-name #'parse-integer)))
    (find id (variants) :key #'id-of)))

(defun main (root)
  (let ((*root* root))
    (with-view (root :root root)
      (dispatch-command
	(child
	 (with-variables ((child (children-of root)))
	   (describe-child child)))
	(new-child
	 (new-child root))
	(parent
	 (with-variables ((parent (nodes-of root)))
	   (if (eq parent root)
	       (main parent)
	       (describe-child parent))))))))

(define-variable (root child)
  (find-object-by-id "child-id"))

(define-variable (root parent)
  (find-object-by-id "node-id"))

(defun describe-child (node)
  (with-view (child :child node)
    (dispatch-command
      (child
       (with-variables ((child (children-of node)))
	 (describe-child child)))
      (new-child
       (new-child node)))))

(define-variable (child child)
  (find-object-by-id "child-id"))

(defun new-child (parent) 
  (with-view (new-node :parent parent)
    (with-variables (name)
      (add-child parent name))))

(define-variable (new-node name)
  (parameter-value "name"))
